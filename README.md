# docker image chart

Overview of how various TheRack.io docker images are stacked.

```mermaid
graph TD;
    gitlab-releaser-arm64;
    linux-kernal-builder-arm64;
    openjdk-zulu-arm64-->java-buildtools-ci-arm64;
    openjdk-zulu-arm64-->confluent-kafka-arm64;

    java-buildtools-ci-arm64-->confluent-kafka-builder-arm64;
    java-buildtools-ci-arm64-->hadoop-builder-arm64;
    java-buildtools-ci-arm64-->aws-glue-data-catalog-client-for-apache-hive-metastore-builder-arm64;

    linux-kernal-builder-arm64-->odroid-n2((odroid-n2));

    hadoop-builder-arm64-->apache-hadoop-bin-arm64((apache-hadoop-bin-arm64));

    confluent-kafka-builder-arm64-->fieldandtimebasedpartitioner-bin-arm64((fieldandtimebasedpartitioner-bin-arm64));


    click gitlab-releaser-arm64 "https://gitlab.com/therackio/build-tools/helper-images/gitlab-releaser-arm64" "link"
    click openjdk-zulu-arm64 "https://gitlab.com/therackio/base-images/openjdk-zulu-arm64" "link"

    click confluent-kafka-arm64 "https://gitlab.com/therackio/big-data/applications/apache-kafka/confluent-kafka-arm64" "link"

    click java-buildtools-ci-arm64 "https://gitlab.com/therackio/build-tools/helper-images/java-buildtools-ci-arm64" "link"
    click confluent-kafka-builder-arm64 "https://gitlab.com/therackio/big-data/builders/confluent-kafka-builder-arm64" "link"
    click hadoop-builder-arm64 "https://gitlab.com/therackio/big-data/builders/hadoop-builder-arm64" "link"
    click aws-glue-data-catalog-client-for-apache-hive-metastore-builder-arm64 "https://gitlab.com/therackio/big-data/builders/aws-glue-data-catalog-client-for-apache-hive-metastore-builder-arm64" "link"
    click linux-kernal-builder-arm64 "https://gitlab.com/therackio/build-tools/helper-images/linux-kernal-builder-arm64" "link"

    click odroid-n2 "https://gitlab.com/therackio/linux-kernels/odroid-n2/-/releases" "link"

    click apache-hadoop-bin-arm64 "https://gitlab.com/therackio/big-data/binaries/apache-hadoop-bin-arm64" "link"
    click fieldandtimebasedpartitioner-bin-arm64 "https://gitlab.com/therackio/big-data/applications/apache-kafka/fieldandtimebasedpartitioner-bin-arm64" "link"
```
